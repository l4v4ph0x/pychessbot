
import os
import platform
import subprocess

from window_manager import window_manager


class screengrab:
    def __init__(self, debug = True):
        self.debug = debug

        if self.debug:
            print(f"{__file__} - __init__()")

    def save_window(self, window_id, to_file):
        if self.debug:
            print(f"{__file__} - save_window({window_id}, {to_file})")

        geometry = window_manager().get_window_geometry(window_id)
        self.save_geometry(geometry, to_file)

    def save_geometry(self, geometry, to_file):
        if self.debug:
            print(f"{__file__} - save_geometry({geometry}, {to_file})")

        if platform.system() == "Linux":
            os.system(f"maim {to_file} --hidecursor -g {geometry['width']}x{geometry['height']}+{geometry['x']}+{geometry['y']}")

        elif platform.system() == "Windows":
            to_file = to_file.replace("/", "\\\\")
            os.system(f"win-dist\\\win-tools\\\screengrab\\Release\\screengrab.exe {to_file} {geometry['x']} {geometry['y']} {geometry['width']} {geometry['height']}")
