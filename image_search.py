import os
import numpy as np
import cv2
import multiprocessing
from threading import Thread
import time

from os import listdir
from os.path import isfile, join

try:
    import Image
except ImportError:
    from PIL import Image

from screengrab import screengrab
from converts import converts


class image_search:
    def __init__(self, debug = True):
        self.debug = debug

        if self.debug:
            print(f"{__file__} - __init__()")

        self.cpu_count = multiprocessing.cpu_count() *2
        self.best_images = {}

    def in_range(self, num, ran, cmp):
        if cmp >= num - (ran // 2) and cmp <= num + (ran // 2):
            return True
        return False

    def search_x_pixel_in_range(self, pix, y, pixel_value, range_from, range_to):
        for x in range(range_from, range_to):
            color = pix[x, y]
            if self.in_range(pixel_value, 40, color):
                return x
        return None

    def check_x_pixels_range(self, pix, y, pixel_value, range_from, range_to):
        missed_count = 0

        for x in range(range_from, range_to):
            color = pix[x, y]
            if not self.in_range(pixel_value, 40, color):
                missed_count += 1

        if missed_count > (range_to - range_from) // 2:
            return False
        return True

    def check_cross_pixels(self, pix, x, y, square_width, color_to_check):
        """
        function that check pixels values if square is full black or white.
        """
        missed_count = 0
        x += 2
        y += 2
        square_width -= 4

        for i in range(0, square_width):
            color = pix[x + i, y + i]
            if not self.in_range(color_to_check, 40, color):
                missed_count += 1

        for i in range(0, square_width):
            color = pix[x + square_width // 2, y + i]
            if not self.in_range(color_to_check, 40, color):
                missed_count += 1

            color = pix[x + i, y + square_width // 2]
            if not self.in_range(color_to_check, 40, color):
                missed_count += 1

        if missed_count > 6:
            return False
        return True


    def find_template_locations(self, image, template):
        points = []

        try:
            w, h = template.shape[::-1]
            res = cv2.matchTemplate(image, template, cv2.TM_CCOEFF_NORMED)
            threshold = 0.75
            loc = np.where(res >= threshold)

            for pt in zip(*loc[::-1]):
                if len([p for p in points if abs(pt[0] - p[0]) < 10 and abs(pt[1] - p[1]) < 10]) == 0:
                    points.append(pt)
                    #cv2.rectangle(im, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)
        except:
            return points

        return points

    def thread_find_template_locations(self, chess, piece, row, column, res_white, res_black, square_width, board_geometry, ret):
        x = column * square_width
        y = row * square_width
        w = square_width
        h = square_width

        crop_img_white = res_white[y:y+h, x:x+w]
        crop_img_black = res_black[y:y+h, x:x+w]

        path = f"images/{chess}/{piece}s"
        files = [f for f in listdir(path) if isfile(join(path, f))]

        # if we have best image then add it to first
        if f"white {piece}s" in self.best_images.keys() and self.best_images[f"white {piece}s"] != "":
            files.append(self.best_images[f"white {piece}s"])
            files = files[::-1]
        else:
            # else create new key
            self.best_images[f"white {piece}s"] = ""

        found_white_piece = False

        # seach white piece
        for file in files:
            template = cv2.imread(f"{path}/{file}", 0)

            points = self.find_template_locations(crop_img_white, template)
            if len(points) > 0:
                # add best image we searched to find faster later
                self.best_images[f"white {piece}s"] = file

                named = converts().xy_to_ah18(board_geometry, (x + square_width // 2, y + square_width // 2))
                ret[f"white {piece}s"].append(named)

                #print(f"white {piece}: {named} {file}")
                found_white_piece = True
                break

        if found_white_piece == False:
            # if we have best image then add it to first
            if f"black {piece}s" in self.best_images.keys() and self.best_images[f"black {piece}s"] != "":
                files.append(self.best_images[f"black {piece}s"])
                files = files[::-1]
            else:
                # else create new key
                self.best_images[f"black {piece}s"] = ""

            # seach black piece
            for file in files:
                template = cv2.imread(f"{path}/{file}", 0)

                points = self.find_template_locations(crop_img_black, template)
                if len(points) > 0:
                    # add best image we searched to find faster later
                    self.best_images[f"black {piece}s"] = file

                    named = converts().xy_to_ah18(board_geometry, (x + square_width // 2, y + square_width // 2))
                    ret[f"black {piece}s"].append(named)

                    break


    def get_board_geometry(self, image_location: str, adder = 2):
        if self.debug:
            print(f"{__file__} - get_board_geometry({image_location}, {adder})")

        start_x = 0
        start_y = 0
        square_width = 0
        square_widths = []
        board_width = 0
        board_height = 0

        # defaults so we dont have to search way too much
        default_square_width = 30
        default_board_width = 8 * default_square_width

        # edit image to find chess board better
        # on linux we can use window opacity and eyecandy stuff
        im = cv2.imread(image_location)
        res = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        res = cv2.GaussianBlur(res, (5, 5), 0)
        th, res = cv2.threshold(res, 172, 255, cv2.THRESH_BINARY)

        #cv2.imwrite("tmp/screen-result.jpg", res)

        #im = Image.open("tmp/screen-result.jpg")
        im = Image.fromarray(res)
        pix = im.load()

        for y in range(40, im.size[1] - default_board_width):
            # if we have got starting pos then
            # no need to look futher
            if square_width != 0:
                break

            # first searching white pixel
            x = 0

            # if x is searching way to far away then cut it
            # cause ches board cant exists like 10% of screen width on right side
            while x < im.size[0] - default_board_width:

                # search for white pixel
                cx = self.search_x_pixel_in_range(pix, y, 255, x, im.size[0])

                # if we found white pixel
                if cx != None:
                    # then set current found place to next search x
                    # so we dont have to just search from x + 1
                    x = cx

                    # also check if white pixels exists at least default_square_width
                    if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 255, cx, cx + default_square_width):
                        # now search black pixel in next 100 pixels after default_square_width pixels
                        nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                        cx = self.search_x_pixel_in_range(pix, y, 0, cx + default_square_width +1, nrts)

                        if cx != None:
                            x = cx

                            # also check if black pixels exists at least default_square_width
                            if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 0, cx, cx + default_square_width):
                                # set black pixel var to find square real width
                                # and now were on 2nd square
                                black_pixel_start = cx

                                # now search white pixel in next 100 pixels after default_square_width pixels
                                nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                cx = self.search_x_pixel_in_range(pix, y, 255, cx + default_square_width +1, nrts)

                                if cx != None:
                                    x = cx

                                    # also check if white pixels exists at least default_square_width
                                    if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 255, cx, cx + default_square_width):
                                        # set white pixel var to find square real width
                                        # and now were on 3rd square
                                        white_pixel_start = cx

                                        # -1 since we're already on white pixel and we want to to know width
                                        # also make new array to set found square widths
                                        #square_widths = [white_pixel_start - black_pixel_start - 1]
                                        square_widths = []

                                        # now search black pixel in next 100 pixels after default_square_width pixels
                                        nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                        cx = self.search_x_pixel_in_range(pix, y, 0, cx + default_square_width +1, nrts)

                                        if cx != None:
                                            x = cx

                                            # also check if white pixels exists at least default_square_width
                                            if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 0, cx, cx + default_square_width):
                                                # set black pixel var to find square real width
                                                # and now were on 4th square
                                                black_pixel_start = cx

                                                # -1 since we're already on white pixel and we want to to know width
                                                # append new square width
                                                square_widths.append(black_pixel_start - white_pixel_start - 1)

                                                # now search white pixel in next 100 pixels after default_square_width pixels
                                                nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                                cx = self.search_x_pixel_in_range(pix, y, 255, cx + default_square_width +1, nrts)

                                                if cx != None:
                                                    x = cx

                                                    # also check if white pixels exists at least default_square_width
                                                    if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 255, cx, cx + default_square_width):
                                                        # set black pixel var to find square real width
                                                        # and now were on 5th square
                                                        white_pixel_start = cx

                                                        # -1 since we're already on white pixel and we want to to know width
                                                        # append new square width
                                                        square_widths.append(white_pixel_start - black_pixel_start - 1)

                                                        # now search black pixel in next 100 pixels after default_square_width pixels
                                                        nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                                        cx = self.search_x_pixel_in_range(pix, y, 0, cx + default_square_width +1, nrts)

                                                        if cx != None:
                                                            x = cx

                                                            # also check if black pixels exists at least default_square_width
                                                            if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 0, cx, cx + default_square_width):

                                                                # set black pixel var to find square real width
                                                                # and now were on 6th square
                                                                black_pixel_start = cx

                                                                # -1 since we're already on white pixel and we want to to know width
                                                                # append new square width
                                                                square_widths.append(black_pixel_start - white_pixel_start - 1)

                                                                # now search white pixel in next 100 pixels after default_square_width pixels
                                                                nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                                                cx = self.search_x_pixel_in_range(pix, y, 255, cx + default_square_width +1, nrts)

                                                                if cx != None:
                                                                    x = cx

                                                                    # also check if white pixels exists at least default_square_width
                                                                    if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 255, cx, cx + default_square_width):

                                                                        # set white pixel var to find square real width
                                                                        # and now were on 7th square
                                                                        white_pixel_start = cx

                                                                        # -1 since we're already on white pixel and we want to to know width
                                                                        # append new square width
                                                                        square_widths.append(white_pixel_start - black_pixel_start - 1)

                                                                        # now search black pixel in next 100 pixels after default_square_width pixels
                                                                        nrts = cx + default_square_width + 101 if cx + default_square_width + 101 <= im.size[0] else im.size[0]
                                                                        cx = self.search_x_pixel_in_range(pix, y, 0, cx + default_square_width +1, nrts)

                                                                        if cx != None:
                                                                            x = cx

                                                                            # also check if black pixels exists at least default_square_width
                                                                            if cx + default_square_width < im.size[0] and self.check_x_pixels_range(pix, y + 2, 0, cx, cx + default_square_width):

                                                                                # set white pixel var to find square real width
                                                                                # and now were on 8th square
                                                                                black_pixel_start = cx

                                                                                # -1 since we're already on white pixel and we want to to know width
                                                                                # append new square width
                                                                                square_widths.append(black_pixel_start - white_pixel_start - 1)

                                                                                print(square_widths)
                                                                                # set board geometry
                                                                                square_width = (sum(square_widths) // len(square_widths)) + adder
                                                                                start_x = black_pixel_start - (square_width * 7)
                                                                                start_y = y -1
                                                                                board_width = square_width * 8
                                                                                board_height = board_width

                                                                                break
                else:
                    # break if we havnt found white pixel in this row
                    # and get new row
                    break

                # increment x by one
                x = x + 1

        # remove modified screen file
        #os.remove("tmp/screen-result.jpg")

        if start_x == 0 and start_y == 0 and board_width == 0 and board_height == 0:
            return None

        return {"x": start_x, "y": start_y, "width": board_width, "height": board_height}

    def get_chess_status(self, window_geometry: dict, board_geometry: dict, chess: str, positions: dict = {}):
        """
        eats up window position and size, chess board position and size to window
            and chess type e.g: lichess

        and returns where pieces exists e.g: {"black pawns": ["a6", "b6"], "white rooks": ["a1", "h1"]}
        """
        if self.debug:
            print(f"{__file__} - get_chess_status({window_geometry}, {board_geometry}, {chess}, {positions})")

        ret = {}

        geometry = {
            "x": window_geometry["x"] + board_geometry["x"],
            "y": window_geometry["y"] + board_geometry["y"],
            "width": board_geometry["width"],
            "height": board_geometry["height"]
        }

        screengrab(debug=self.debug).save_geometry(geometry, "tmp/screen.jpg")

        # modify screen image to search white pieces
        im_white = cv2.imread("tmp/screen.jpg")
        im_grey = cv2.cvtColor(im_white, cv2.COLOR_BGR2GRAY)
        res_white = cv2.GaussianBlur(im_grey, (5, 5), 0)
        th, res_white = cv2.threshold(res_white, 170, 255, cv2.THRESH_BINARY_INV)

        # convert to pillow image
        im_white = Image.fromarray(res_white)
        pix_white = im_white.load()

        #cv2.imwrite("tmp/screen-result.jpg", res_white)

        # modify screen image to search black pieces
        res_black = cv2.GaussianBlur(im_grey, (5, 5), 0)
        th, res_black = cv2.threshold(res_black, 51, 255, cv2.THRESH_BINARY)

        # convert to pillow image
        im_black = Image.fromarray(res_black)
        pix_black = im_black.load()

        #cv2.imwrite("tmp/screen-result.jpg", res)

        pieces = ["pawn", "rook", "knight", "bishop", "queen", "king"]
        square_width = board_geometry["width"] // 8
        threads = []

        rows_to_check = [0, 1, 2, 3, 4, 5, 6, 7]
        columns_to_check = [0, 1, 2, 3, 4, 5, 6, 7]
        rc_not_to_check = []

        # check if pieces that existsied before exists still
        if positions:
            for k in positions.keys():
                for pos in positions[k]:
                    x, y = converts(debug=False).ah18_to_xy(board_geometry, pos)
                    w, h = square_width, square_width

                    crop_img = (res_white if "white" in k else res_black)[y:y+h, x:x+w]
                    path = f"images/{chess}/{k.split(' ')[1]}"
                    files = [f for f in listdir(path) if isfile(join(path, f))]

                    # get best image we found this piece and place it to first
                    if k in self.best_images.keys():
                        files.append(self.best_images[k])
                        files = files[::-1]

                    for file in files:
                        template = cv2.imread(f"{path}/{file}", 0)

                        # check if frendly piece exists on that square
                        points = self.find_template_locations(crop_img, template)
                        if len(points) > 0:
                            # add ignore row and column to check next time
                            rc_not_to_check.append((x // square_width, y // square_width))
                            # and add to ret
                            if k not in ret.keys():
                                ret[k] = [pos]
                            else:
                                ret[k].append(pos)

                            break

        for piece in pieces:
            if f"white {piece}s" not in ret:
                ret[f"white {piece}s"] = []

            if f"black {piece}s" not in ret:
                ret[f"black {piece}s"] = []

            for row in rows_to_check:
                for column in columns_to_check:
                    # first check if this square is empty
                    #empty = False
                    #if self.check_cross_pixels(pix_black, column, row, square_width, 255):
                    #    empty = True
                    #if empty and (self.check_cross_pixels(pix_white, column, row, square_width, 0) or self.check_cross_pixels(pix_white, column, row, square_width, 255)):
                    #    continue


                    if (column, row) in rc_not_to_check:
                        #print(f"noice {(column, row)}")
                        continue

                    while len(threads) >= self.cpu_count:
                        threads = [t for t in threads if t.is_alive()]
                        time.sleep(0.01)

                    thread = Thread(target=self.thread_find_template_locations, args=(chess, piece, row, column, res_white, res_black, square_width, board_geometry, ret))
                    thread.start()
                    threads.append(thread)

                    #cv2.imwrite(f"tmp/re{row}-{column}.jpg", crop_img_white)
                    #cv2.imwrite(f"tmp/re{row}-{column}.jpg", crop_img_black)


        # remove screenshot we took
        #os.remove("tmp/screen.jpg")

        # let all threads end adnd then return
        while len(threads) >= 1:
            threads = [t for t in threads if t.is_alive()]

        return ret

    def check_pieces_movement(self, window_geometry: dict, board_geometry: dict, chess: str, positions: dict):
        """
        eats up window position and size, chess board position and size to window
            and chess type e.g: lichess

        return position that does not exist anymore
        """
        if self.debug:
            print(f"{__file__} - check_pieces_movement({window_geometry}, {board_geometry}, {chess}, {positions})")

        ret = None

        geometry = {
            "x": window_geometry["x"] + board_geometry["x"],
            "y": window_geometry["y"] + board_geometry["y"],
            "width": board_geometry["width"],
            "height": board_geometry["height"]
        }

        screengrab(debug=self.debug).save_geometry(geometry, "tmp/screen.jpg")

        # modify screen image to search white pieces
        im_white = cv2.imread("tmp/screen.jpg")
        im_grey = cv2.cvtColor(im_white, cv2.COLOR_BGR2GRAY)
        res_white = cv2.GaussianBlur(im_grey, (5, 5), 0)
        th, res_white = cv2.threshold(res_white, 170, 255, cv2.THRESH_BINARY_INV)

        # convert to pillow image
        im_white = Image.fromarray(res_white)
        pix_white = im_white.load()

        # modify screen image to search black pieces
        res_black = cv2.GaussianBlur(im_grey, (5, 5), 0)
        th, res_black = cv2.threshold(res_black, 51, 255, cv2.THRESH_BINARY)

        # convert to pillow image
        im_black = Image.fromarray(res_black)
        pix_black = im_black.load()

        pieces = ["pawn", "rook", "knight", "bishop", "queen", "king"]
        square_width = board_geometry["width"] // 8
        threads = []

        for k in positions.keys():
            if ret != None:
                break

            for pos in positions[k]:
                x, y = converts(debug=False).ah18_to_xy(board_geometry, pos)
                w, h = square_width, square_width

                crop_img_white = res_white[y:y+h, x:x+w]
                crop_img_black = res_black[y:y+h, x:x+w]

                path = f"images/{chess}/{k.split(' ')[1]}"
                files = [f for f in listdir(path) if isfile(join(path, f))]

                # get best image we found this piece and place it to first
                if k in self.best_images.keys():
                    files.append(self.best_images[k])
                    files = files[::-1]

                piece_exists = False
                for file in files:
                    template = cv2.imread(f"{path}/{file}", 0)

                    # check if frendly piece exists on that square
                    points = self.find_template_locations(crop_img_black if "black" in k else crop_img_white, template)
                    if len(points) > 0:
                        piece_exists = True
                        break

                if piece_exists == False:
                    ret = pos
                    break


        return ret
