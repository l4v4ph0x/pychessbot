import os
import sys
import platform
from distutils.spawn import find_executable

tools_we_use_in_linux = ["maim", "xdotool"]
tools_we_use_in_windows = ["cmd"]

def check_tool(name):
    if find_executable(name) is None:
        print(f"ERROR - {__file__} missing tool {name}")
        return False
    return True

if __name__ == '__main__':
    got_all_tools = False
    supported_platform = False

    if platform.system() == "Linux":
        supported_platform = True

        for tool in tools_we_use_in_linux:
            got_all_tools = check_tool(tool)

            # if we got even one tool missing then break
            if got_all_tools == False:
                break

        if got_all_tools:
            # build uis
            os.system("pyuic5 mainDialog.ui -o ui_mainDialog.py")
            os.system("pyuic5 enginesDialog.ui -o ui_enginesDialog.py")

    # and if we're gonna use windows machine
    elif platform.system() == "Windows":
        supported_platform = True

        for tool in tools_we_use_in_windows:
            got_all_tools = check_tool(tool)

            # if we got even one tool missing then break
            if got_all_tools == False:
                break

        if got_all_tools:
            # build uis
            os.system("win-dist\\Python3\\Python.exe win-dist\\Python3\\Scripts\\pyuic5.py mainDialog.ui -o ui_mainDialog.py")
            os.system("win-dist\\Python3\\Python.exe win-dist\\Python3\\Scripts\\pyuic5.py enginesDialog.ui -o ui_enginesDialog.py")

    if supported_platform == False:
        print(f"ERROR - {__file__} - __main__: {platform.system()} is not supported")
        sys.exit(0)

    if got_all_tools == False:
        print(f"ERROR - {__file__} - __main__: missing some tool")
        sys.exit(0)

    # and import out lil main dialog
    from dialog_main import dialog_main

    #print(window_names)
    dialog_main(sys.argv)
    sys.exit(0)
