# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainDialog.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_mainDialog(object):
    def setupUi(self, mainDialog):
        mainDialog.setObjectName("mainDialog")
        mainDialog.setEnabled(True)
        mainDialog.resize(630, 203)
        self.lb_selectWindow = QtWidgets.QLabel(mainDialog)
        self.lb_selectWindow.setGeometry(QtCore.QRect(10, 60, 101, 18))
        self.lb_selectWindow.setObjectName("lb_selectWindow")
        self.cb_selectWindow = QtWidgets.QComboBox(mainDialog)
        self.cb_selectWindow.setGeometry(QtCore.QRect(110, 50, 511, 31))
        self.cb_selectWindow.setObjectName("cb_selectWindow")
        self.lb_status = QtWidgets.QLabel(mainDialog)
        self.lb_status.setGeometry(QtCore.QRect(10, 180, 611, 20))
        self.lb_status.setObjectName("lb_status")
        self.lb_playingAs = QtWidgets.QLabel(mainDialog)
        self.lb_playingAs.setEnabled(False)
        self.lb_playingAs.setGeometry(QtCore.QRect(10, 100, 71, 18))
        self.lb_playingAs.setObjectName("lb_playingAs")
        self.cb_playingAs = QtWidgets.QComboBox(mainDialog)
        self.cb_playingAs.setEnabled(False)
        self.cb_playingAs.setGeometry(QtCore.QRect(90, 90, 121, 32))
        self.cb_playingAs.setObjectName("cb_playingAs")
        self.lb_fame = QtWidgets.QLabel(mainDialog)
        self.lb_fame.setGeometry(QtCore.QRect(87, 10, 471, 20))
        self.lb_fame.setObjectName("lb_fame")
        self.lb_bestMove = QtWidgets.QLabel(mainDialog)
        self.lb_bestMove.setGeometry(QtCore.QRect(10, 140, 151, 18))
        self.lb_bestMove.setObjectName("lb_bestMove")
        self.lb_stockfishLevel = QtWidgets.QLabel(mainDialog)
        self.lb_stockfishLevel.setEnabled(False)
        self.lb_stockfishLevel.setGeometry(QtCore.QRect(220, 100, 101, 18))
        self.lb_stockfishLevel.setObjectName("lb_stockfishLevel")
        self.cb_stockfishLevel = QtWidgets.QComboBox(mainDialog)
        self.cb_stockfishLevel.setEnabled(False)
        self.cb_stockfishLevel.setGeometry(QtCore.QRect(320, 90, 71, 32))
        self.cb_stockfishLevel.setObjectName("cb_stockfishLevel")
        self.btn_reFindBoard = QtWidgets.QPushButton(mainDialog)
        self.btn_reFindBoard.setEnabled(False)
        self.btn_reFindBoard.setGeometry(QtCore.QRect(437, 90, 181, 34))
        self.btn_reFindBoard.setObjectName("btn_reFindBoard")
        self.lb_stockfishDepth = QtWidgets.QLabel(mainDialog)
        self.lb_stockfishDepth.setEnabled(False)
        self.lb_stockfishDepth.setGeometry(QtCore.QRect(210, 150, 101, 18))
        self.lb_stockfishDepth.setObjectName("lb_stockfishDepth")
        self.cb_stockfishDepth = QtWidgets.QComboBox(mainDialog)
        self.cb_stockfishDepth.setEnabled(False)
        self.cb_stockfishDepth.setGeometry(QtCore.QRect(320, 140, 71, 32))
        self.cb_stockfishDepth.setObjectName("cb_stockfishDepth")
        self.btn_engines = QtWidgets.QPushButton(mainDialog)
        self.btn_engines.setGeometry(QtCore.QRect(590, 10, 31, 34))
        self.btn_engines.setObjectName("btn_engines")

        self.retranslateUi(mainDialog)
        QtCore.QMetaObject.connectSlotsByName(mainDialog)

    def retranslateUi(self, mainDialog):
        _translate = QtCore.QCoreApplication.translate
        mainDialog.setWindowTitle(_translate("mainDialog", "Dialog"))
        self.lb_selectWindow.setText(_translate("mainDialog", "Select Window"))
        self.lb_status.setText(_translate("mainDialog", "Status:"))
        self.lb_playingAs.setText(_translate("mainDialog", "Playing As"))
        self.lb_fame.setText(_translate("mainDialog", "This little piece of software is produced by lava and licensed under LGPL v2.1"))
        self.lb_bestMove.setText(_translate("mainDialog", "Best Move:"))
        self.lb_stockfishLevel.setText(_translate("mainDialog", "Stockfish Level"))
        self.btn_reFindBoard.setText(_translate("mainDialog", "Find Chess Board Again"))
        self.lb_stockfishDepth.setText(_translate("mainDialog", "Stockfish Depth"))
        self.btn_engines.setText(_translate("mainDialog", "E"))


