#include "../hh/summoner.h"
#include <string>

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
std::vector<char *> foundWindowNames;
char *_needle;

bool FindWindowsContatining(char *needle, std::vector<char *> *longName) {
  _needle = needle;
  foundWindowNames.clear();

  EnumWindows(EnumWindowsProc, NULL);

  Sleep(100);
  *longName = foundWindowNames;

  if (foundWindowNames.size() == 0) {
    return false;
  }

  return true;
}

BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam) {
  char class_name[256];
  char *title = (char *)malloc(255);

  GetClassName(hwnd, class_name, sizeof(class_name));
  GetWindowText(hwnd, title, 255);

  if (std::string(title).find(std::string(_needle)) != std::string::npos) {
    for (int i = 0; i < foundWindowNames.size(); i++) {
      if (strcmp(foundWindowNames[i], title) == 0) {
        free(title);
        return TRUE;
      }
    }

    foundWindowNames.push_back(title);
  } else {
    free(title);
  }
  
  return TRUE;
}
