#include <stdio.h>
#include <windows.h>

int main(int argc, char *argv[]) {
  HWND hwnd = FindWindowA(0, (char *)argv[1]);

  if (hwnd) {
    RECT rc;
    GetWindowRect(hwnd, &rc);

    printf("Window %s\n  Position: %d,%d (screen: %s)\n  Geometry: %dx%d\n", 
      argv[1], rc.left, rc.top, "not implemented", rc.right - rc.left, rc.bottom - rc.top);
  }
}
