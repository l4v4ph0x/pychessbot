# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'enginesDialog.ui'
#
# Created by: PyQt5 UI code generator 5.12
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_enginesDialog(object):
    def setupUi(self, enginesDialog):
        enginesDialog.setObjectName("enginesDialog")
        enginesDialog.resize(308, 113)
        self.lb_engine = QtWidgets.QLabel(enginesDialog)
        self.lb_engine.setGeometry(QtCore.QRect(70, 10, 151, 18))
        self.lb_engine.setObjectName("lb_engine")
        self.cb_engines = QtWidgets.QComboBox(enginesDialog)
        self.cb_engines.setGeometry(QtCore.QRect(10, 30, 291, 31))
        self.cb_engines.setObjectName("cb_engines")
        self.btn_ok = QtWidgets.QPushButton(enginesDialog)
        self.btn_ok.setGeometry(QtCore.QRect(250, 70, 51, 34))
        self.btn_ok.setObjectName("btn_ok")

        self.retranslateUi(enginesDialog)
        QtCore.QMetaObject.connectSlotsByName(enginesDialog)

    def retranslateUi(self, enginesDialog):
        _translate = QtCore.QCoreApplication.translate
        enginesDialog.setWindowTitle(_translate("enginesDialog", "Dialog"))
        self.lb_engine.setText(_translate("enginesDialog", "Select Stockfish Engine"))
        self.btn_ok.setText(_translate("enginesDialog", "OK"))


