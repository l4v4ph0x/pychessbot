
import os
import platform
import subprocess

class window_manager:
    def __init__(self, debug = True):
        self.debug = debug

        if self.debug:
            print(f"{__file__} - __init__({debug})")

    def find_windows(self, names: list):
        if self.debug:
            print(f"{__file__} - find_windows({names})")

        ret = []

        if platform.system() == "Linux":
            for name in names:
                window_ids = os.popen(f"xdotool search --onlyvisible --name {name}", "r", 1).read()
                for window_id in window_ids.split("\n"):
                    if window_id != "":
                        window_name = os.popen(f"xdotool getwindowname {window_id}", "r", 1).read()
                        ret.append({"id": window_id, "name": window_name.replace("\n", "")})

        elif platform.system() == "Windows":
            for name in names:
                window_ids = os.popen(f"win-dist\\win-tools\\findWindow\\Release\\findWindow.exe {name}", "r", 1).read()
                for window_id in window_ids.split("\n"):
                    if window_id != "":
                        ret.append({"id": window_id.replace("\n", ""), "name": window_id.replace("\n", "")})

        return ret

    def get_window_geometry(self, window_id):
        if self.debug:
            print(f"{__file__} - get_window_geometry({window_id})")

        ret = None

        if platform.system() == "Linux":
            b = os.popen(f"xdotool getwindowgeometry {window_id}", "r", 1).read()

        elif platform.system() == "Windows":
            b = os.popen(f"win-dist\\win-tools\\getWindowGeometry\\Release\\getWindowGeometry.exe \"{window_id}\"", "r", 1).read()

        # else return empty
        else:
            return ret

        pos = b.split("Position: ")[1].split(" ")[0].split(",")
        size = b.split("Geometry: ")[1].split(" ")[0].replace("\n", "").split("x")

        ret = {
            "x": int(pos[0]), "y": int(pos[1]),
            "width": int(size[0]), "height": int(size[1])
        }

        return ret

    def bring_window_up(self, window_id):
        if self.debug:
            print(f"{__file__} - bring_window_up({window_id})")

        if platform.system() == "Linux":
            os.system(f"xdotool windowraise {window_id}")
            os.system(f"xdotool windowactivate {window_id}")

        elif platform.system() == "Windows":
            os.system(f"win-dist\\win-tools\\bringWindowUp\\Release\\bringWindowUp.exe \"{window_id}\"")
