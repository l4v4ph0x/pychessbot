import os
import _thread
import time
import platform

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot

from ui_mainDialog import Ui_mainDialog
from dialog_engines import dialog_engines

from window_manager import window_manager
from screengrab import screengrab
from image_search import image_search
from converts import converts

from pystockfish import *

class dialog_main:
    def __init__(self, argv):
        print(f"{__file__} - __init__({argv})")

        app = QApplication(argv)
        window = QDialog()
        ui = Ui_mainDialog()
        ui.setupUi(window)

        if platform.system() == "Linux":
            self.engine = "engines/stockfish/stockfish_10_x64"
        elif platform.system() == "Windows":
            self.engine = "engines/stockfish/stockfish_10_x64.exe"

        self.windows = []
        self.selected_window = ""
        self.window_geometry = {}
        self.board_geometry = {}
        self.status = {}
        self.playing_as = "w"
        self.fen = ""
        self.thinking = False
        self.depth = 18

        self.clear_local_vars(ui)

        ui.cb_selectWindow.addItem("Select window to find board table")

        # add cb playing as values
        ui.cb_playingAs.addItem("White")
        ui.cb_playingAs.addItem("Black")

        # add stockfish levels
        for i in range(1, 21):
            ui.cb_stockfishLevel.addItem(str(i))
        # also set default level also
        ui.cb_stockfishLevel.setCurrentIndex(19)

        # add stockfish depths
        for i in range(1, 41):
            ui.cb_stockfishDepth.addItem(str(i))
        # also set default depth also
        ui.cb_stockfishDepth.setCurrentIndex(17)

        # set signals
        ui.btn_engines.clicked.connect(lambda: self.btn_engines_clicked(ui))
        ui.cb_selectWindow.currentIndexChanged.connect(lambda x: self.cb_selectWindow_currentIndexChanged(ui, x))
        ui.cb_playingAs.currentIndexChanged.connect(lambda x: self.cb_playingAs_currentIndexChanged(ui, x))
        ui.cb_stockfishLevel.currentIndexChanged.connect(lambda x: self.cb_stockfishLevel_currentIndexChanged(ui, x))
        ui.btn_reFindBoard.clicked.connect(lambda: self.btn_reFindBoard_clicked(ui))
        ui.cb_stockfishDepth.currentIndexChanged.connect(lambda x: self.cb_stockfishDepth_currentIndexChanged(ui, x))

        # start threads
        _thread.start_new_thread(self.thread_check_chess_windows, (ui, ))

        window.show()
        app.exec_()

    def thread_check_chess_windows(self, ui):
        print(f"{__file__} - thread_check_chess_windows({ui})")

        old_windows = []

        while True:
            self.windows = window_manager(debug=False).find_windows(["lichess", "chess.com"])
            if len(self.windows) != len(old_windows):

                # add new names to combo box that does not exists yet
                for w in self.windows:
                    if w["id"] not in [wi["id"] for wi in old_windows]:
                        ui.cb_selectWindow.addItem(w["name"])

                # get current combob box window names
                windows = []
                for i in range(ui.cb_selectWindow.count()):
                    windows.append(ui.cb_selectWindow.itemText(i))

                # remove window titles that does not exists in self.windows
                for wi in old_windows:
                    if wi["id"] not in [w["id"] for w in self.windows]:
                        ui.cb_selectWindow.removeItem(windows.index(wi["name"]))

                        # if we're removing selected window title then clear local vars
                        if wi["name"] == self.selected_window:
                            ui.cb_selectWindow.setCurrentIndex(0)
                            self.clear_local_vars(ui)

                old_windows = self.windows

            # check windows every 1 second
            time.sleep(0.5)


    def thread_search_board_geometry(self, ui, index):
        print(f"{__file__} - thread_search_board_geometry({ui}, {index})")

        # take screenshot of window and search board geometry
        screengrab().save_window(self.windows[index -1]["id"], "tmp/screen.jpg")
        self.board_geometry = image_search().get_board_geometry("tmp/screen.jpg")

        # remove tmp screenshot afterwards
        os.remove("tmp/screen.jpg")

        if self.board_geometry == None:
            print(f"{__file__} - thread_search_board_geometry: failed to find chess board")
            ui.lb_status.setText(f"failed to find chess board")
        else:
            print(f"{__file__} - thread_search_board_geometry: found chess board {self.board_geometry}, going to search chess status")
            ui.lb_status.setText(f"Status: board geometry: {self.board_geometry}, analysing chess status")

            # activate contols
            self.enable_controls(ui)

            # create new thread to search wut status of chess
            _thread.start_new_thread(self.thread_search_chess_status, (ui, lambda: self.get_best_move(ui, debug=False)))


    def thread_search_chess_status(self, ui, callback):
        print(f"{__file__} - thread_search_chess_status({ui}, {callback})")

        # create and hold image search class cause it learns to find faster after first time
        imse = image_search(debug=False)

        changed_counter = 0

        while self.selected_window != "":
            # this troubles really much
            #window_manager(debug=False).bring_window_up(self.windows[ui.cb_selectWindow.currentIndex() -1]["id"])

            # check if window has maved and check if its stabilizes
            #while True:
            #    window_geometry = window_manager(debug=False).get_window_geometry(self.windows[ui.cb_selectWindow.currentIndex() -1]["id"])
            #    if self.window_geometry["x"] == window_geometry["x"] and self.window_geometry["y"] == window_geometry["y"]:
            #        break
            #
            #    self.window_geometry = window_geometry
            #    time.sleep(1)

            # if we havent even got first chess status
            if "/" not in self.fen:
                self.status = imse.get_chess_status(self.window_geometry, self.board_geometry, "lichess")
            else:
                enemy_str = "black" if self.playing_as == "w" else "white"
                friendly_str = "black" if self.playing_as == "b" else "white"

                enemy_status = {k:self.status[k] for k in self.status.keys() if enemy_str in k}
                friendly_status = {k:self.status[k] for k in self.status.keys() if friendly_str in k}

                # check if enemy is moved or removed piece
                enemy_moved = imse.check_pieces_movement(self.window_geometry, self.board_geometry, "lichess", enemy_status)
                if enemy_moved != None:
                    # let piece to move if any, for better image res after
                    time.sleep(0.2)
                    self.status = imse.get_chess_status(self.window_geometry, self.board_geometry, "lichess", self.status)

                    # check if white piece is now on where black got lost
                    fiendly_ate_this_pos = False
                    for k in self.status:
                        if friendly_str in k:
                            if enemy_moved in self.status[k]:
                                fiendly_ate_this_pos = True
                                break

                    if fiendly_ate_this_pos:
                        time.sleep(0.1)
                        continue
                else:
                    time.sleep(0.1)
                    continue


            # also check if we really updated fen
            if self.update_fen(ui, debug=False):
                print(self.fen)
                callback()

    def thread_get_best_move(self, ui, debug):
        if debug:
            print(f"{__file__} - thread_get_best_move({ui}, {debug})")

        str = ""

        self.deep = Engine(depth=self.depth, engine=self.engine)
        self.deep.setfenposition(self.fen)
        time.sleep(1)
        #self.deep.setposition(['e2e4'])
        best_move = self.deep.bestmove()

        # if pystockfish failes to read output or something then re do it
        if best_move == None:
            self.thread_get_best_move(ui, debug)
            return

        str = f"Best Move: {best_move}"

        ui.lb_bestMove.show()
        ui.lb_bestMove.setText(str)

        # some weird things happen and just recheck
        time.sleep(0.5)
        if ui.lb_bestMove.text() == "Best Move: thinking":
            ui.lb_bestMove.setText(str)

        self.thinking = False


    def update_fen(self, ui, debug = True):
        if debug:
            print(f"{__file__} - update_fen({ui})")

        if not self.status:
            print(f"ERROR - {__file__} - update_fen: empty `self.status`")
        else:
            fen = converts(debug=debug).image_search_status_to_fen(self.status) + f" {self.playing_as} KQkq -"
            if self.playing_as != "w":
                fen = converts(debug=debug).flip_fen(fen)

            if self.fen != fen:
                self.fen = fen

                ui.lb_status.setText(f"Status: fen = {self.fen}")

                return True

        return False



    def enable_controls(self, ui):
        print(f"{__file__} - enable_controls({ui})")

        ui.lb_playingAs.setEnabled(True)
        ui.cb_playingAs.setEnabled(True)
        ui.lb_stockfishLevel.setEnabled(True)
        ui.cb_stockfishLevel.setEnabled(True)
        ui.btn_reFindBoard.setEnabled(True)
        ui.lb_stockfishDepth.setEnabled(True)
        ui.cb_stockfishDepth.setEnabled(True)
        # gonna show it when we press get best move button
        #ui.lb_bestMove.show()

    def clear_local_vars(self, ui):
        print(f"{__file__} - clear_local_vars({ui})")

        ui.lb_playingAs.setEnabled(False)
        ui.cb_playingAs.setEnabled(False)
        ui.lb_stockfishLevel.setEnabled(False)
        ui.cb_stockfishLevel.setEnabled(False)
        ui.btn_reFindBoard.setEnabled(False)
        ui.lb_stockfishDepth.setEnabled(False)
        ui.cb_stockfishDepth.setEnabled(False)
        ui.lb_bestMove.hide()
        ui.lb_status.setText(f"Status:")

        self.selected_window = ""
        self.window_geometry = {}
        self.board_geometry = {}
        self.status = {}
        self.thinking = False

    def btn_engines_clicked(self, ui):
        print(f"{__file__} - btn_engines_clicked({ui})")
        dialog_engines(self)

    @pyqtSlot(int)
    def cb_selectWindow_currentIndexChanged(self, ui, index):
        print(f"{__file__} - cb_selectWindow_currentIndexChanged({ui}, {index})")

        if index != 0:
            self.selected_window = ui.cb_selectWindow.itemText(index)
            window_manager().bring_window_up(self.windows[index -1]["id"])
            self.window_geometry = window_manager().get_window_geometry(self.windows[index -1]["id"])

            ui.lb_status.setText("Status: searching chess board")
            _thread.start_new_thread(self.thread_search_board_geometry, (ui, index))
        else:
            self.clear_local_vars(ui)

    @pyqtSlot(int)
    def cb_playingAs_currentIndexChanged(self, ui, index):
        print(f"{__file__} - cb_selectWindow_currentIndexChanged({ui}, {index})")

        if index == 0:
            self.playing_as = "w"
        elif index == 1:
            self.playing_as = "b"

        self.update_fen(ui)
        self.get_best_move(ui, debug=False)

    @pyqtSlot(int)
    def cb_stockfishLevel_currentIndexChanged(self, ui, index):
        print(f"{__file__} - cb_stockfishLevel_currentIndexChanged({ui}, {index})")

        # set stockfish level just index + 1
        self.deep.setoption("Skill Level", index +1)

    def btn_reFindBoard_clicked(self, ui):
        print(f"{__file__} - btn_reFindBoard_clicked({ui})")

        # close thread that searching chess status
        self.selected_window = ""
        # and simulate re selecting window to load searching cenario again
        self.cb_selectWindow_currentIndexChanged(ui, ui.cb_selectWindow.currentIndex())

    @pyqtSlot(int)
    def cb_stockfishDepth_currentIndexChanged(self, ui, index):
        print(f"{__file__} - cb_stockfishDepth_currentIndexChanged({ui}, {index})")

        # set stockfish depth just index + 1
        self.depth = index +1

    def get_best_move(self, ui, debug = True):
        if debug:
            print(f"{__file__} - get_best_move({ui})")

        ui.lb_bestMove.show()
        ui.lb_bestMove.setText(f"Best Move: thinking")
        self.thinking = True
        _thread.start_new_thread(self.thread_get_best_move, (ui, debug))
