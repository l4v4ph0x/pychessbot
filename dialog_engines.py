from os import listdir
from os.path import isfile, join

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot

from ui_enginesDialog import Ui_enginesDialog

class dialog_engines:
    def __init__(self, dialog_main):
        print(f"{__file__} - __init__({dialog_main})")

        self.dialog_main = dialog_main
        self.window = QDialog()
        self.window.ui = Ui_enginesDialog()
        self.window.ui.setupUi(self.window)
        self.ui = self.window.ui

        # add enigne names to combo box
        self.engines_path = "engines/stockfish"
        self.engines = [f for f in listdir(self.engines_path) if isfile(join(self.engines_path, f))]
        for i, engine in enumerate(self.engines):
            self.ui.cb_engines.addItem(engine)

            if self.dialog_main.engine == self.engines_path + "/" + engine:
                self.ui.cb_engines.setCurrentIndex(i)

        # if engine isnt found then make first selected one
        if self.dialog_main.engine not in [self.engines_path + "/" + e for e in self.engines]:
            self.dialog_main.engine = self.engines[0]


        # add signals
        self.ui.cb_engines.currentIndexChanged.connect(lambda x: self.cb_engines_currentIndexChanged(x))
        self.ui.btn_ok.clicked.connect(lambda: self.btn_ok_clicked())

        self.window.exec_()

    @pyqtSlot(int)
    def cb_engines_currentIndexChanged(self, index):
        print(f"{__file__} - cb_engines_currentIndexChanged({index})")
        self.dialog_main.engine = self.engines_path + "/" + self.engines[index]

    def btn_ok_clicked(self):
        print(f"{__file__} - btn_ok_clicked()")
        self.window.accept()
