
class converts:
    def __init__(self, debug = True):
        self.debug = debug
        pass

    def xy_to_ah18(self, board_geometry: dict, xy_position: tuple):
        ai = int(xy_position[0] // (board_geometry["width"] / 8))
        bi = int(xy_position[1] // (board_geometry["height"] / 8))
        a = "abcdefgh"[ai]
        b = "87654321"[bi]
        return f"{a}{b}"

    def ah18_to_xy(self, board_geometry: dict, ah18_position: str):
        char = ah18_position[0]
        num = ah18_position[1]

        try:
            x = ["a", "b", "c", "d", "e", "f", "g", "h"].index(char) * (board_geometry["width"] // 8)
            y = ["8", "7", "6", "5", "4", "3", "2", "1"].index(num) * (board_geometry["height"] // 8)
        except:
            print(ah18_position)
            print(char)
            print(num)

            raise ValueError("1234")
        return (x, y)

    def flip_fen(self, fen):
        ret = ""
        ffs = fen.split("/")

        for i in [7, 6, 5, 4, 3, 2, 1, 0]:
            ret += "".join(list(ffs[i])[::-1]) + ("/" if i != 0 else "")

        ret += ffs[8]
        return ret


    def image_search_status_to_fen(self, status):
        if self.debug:
            print(f"{__file__} - image_search_status_tp_fen({status})")

        names_to_fen = {
            "white pawns": "P",
            "black pawns": "p",
            "white rooks": "R",
            "black rooks": "r",
            "white knights": "N",
            "black knights": "n",
            "white bishops": "B",
            "black bishops": "b",
            "white queens": "Q",
            "black queens": "q",
            "white kings": "K",
            "black kings": "k"
        }

        fen = ""

        for row in range(1, 9)[::-1]:
            for column in ["a", "b", "c", "d", "e", "f", "g", "h"]:
                piece_name = [k for k in status.keys() if f"{column}{row}" in status[k]]
                if len(piece_name) > 0:
                    fen += names_to_fen[piece_name[0]]
                else:
                    fen += "1"
            fen += "/"

        for i in range(2, 9)[::-1]:
            fen = fen.replace(i * "1", str(i))

        return fen

    def get_rid_of_nested_lists(self, nested_list: list):
        if self.debug:
            print(f"{__file__} - get_rid_of_nested_lists({nested_list})")

        ret = []
        for i in nested_list:
            for j in i:
                ret.append(j)

        return ret
